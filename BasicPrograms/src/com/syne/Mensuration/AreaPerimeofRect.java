//Given the length and breadth of a rectangle, write a program to find whether the area of the rectangle is greater than its perimeter. 
//For example, the area of the rectangle with length = 5 and breadth = 4 is greater than its perimeter.
package com.syne.Mensuration;

import java.util.Scanner;

public class AreaPerimeofRect {

	public static void main(String[] args) {
		int length,bredth,area,peri;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length");
		length = sc.nextInt();
		System.out.println("Enter bredth");
		bredth = sc.nextInt();
		area = length * bredth; 
		System.out.println("Area = "+area);
		peri = length+bredth;
		if(area>peri)
			System.out.println("Area of rectangle is greater than it's parameter");
		else
			System.out.println("Area of rectangle is smaller than it's parameter");
	}

}
