package com.syne.Mensuration;

import java.util.Scanner;

public class Circle {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double radius, area;
		System.out.println("Enter radius of circle");
		radius = sc.nextDouble();
		area = 2*3.14*radius;
		System.out.println("Area = "+area);
	}

}
