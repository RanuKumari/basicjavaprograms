package com.syne.Mensuration;

import java.util.Scanner;

public class Cube {

	public static void main(String[] args) {
		Scanner sc =  new Scanner(System.in);
		int s, area;
		System.out.println("Enter side of cube: ");
		s = sc.nextInt();
		area = 6*s*s;
		System.out.println("Area = "+area);
	}

}
