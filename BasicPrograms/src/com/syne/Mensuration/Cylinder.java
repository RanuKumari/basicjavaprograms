package com.syne.Mensuration;

import java.util.Scanner;

public class Cylinder {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double radius, area,height;
		System.out.println("Enter radius ");
		radius = sc.nextDouble();
		System.out.println("Enter height ");
		height = sc.nextDouble();
		area = 2*3.14*radius*height;
		System.out.println("Area = "+area);
	}

}
