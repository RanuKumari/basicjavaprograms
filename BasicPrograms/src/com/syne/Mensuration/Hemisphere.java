package com.syne.Mensuration;

import java.util.Scanner;

public class Hemisphere {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double radius, area;
		System.out.println("Enter radius ");
		radius = sc.nextDouble();
		area = 3*3.14*radius*radius;
		System.out.println("Area = "+area);
	}

}
