package com.syne.Mensuration;

import java.util.Scanner;

public class Rhombus {

	public static void main(String[] args) {
		double diag1,diag2,area;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter diagonal1 of Rhombus ");
		diag1=sc.nextDouble();
		diag2=sc.nextDouble();
		area = 0.5*diag1*diag2;
		System.out.println("Area = "+area);
	}

}
