package com.syne.Mensuration;

import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		int side, area;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter side");
		side = sc.nextInt();
		area = side*side;
		System.out.println("Area = "+area);
	}

}
