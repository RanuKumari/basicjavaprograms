package com.syne.Mensuration;

import java.util.Scanner;

public class Trapezium {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double s1,s2,h,area;
		System.out.println("Enter 2 parallel sides of trapezium: ");
		s1 = sc.nextDouble();
		s2 = sc.nextDouble();
		System.out.println("Enter altitude of trapezium: ");
		h = sc.nextDouble();
		
		area = 0.5*h*(s1+s2);
		System.out.println("Area = "+area);

	}

}
