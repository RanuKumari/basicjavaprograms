//Find the absolute value of a number entered through the keyboard.
package com.syne.Selection;

import java.util.Scanner;

public class AbsoluteValue {

	public static void main(String[] args) {
		
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number:");
		num=sc.nextInt();
		int absval = Math.abs(num);
		
		System.out.println("Absolute value: "+absval);
	}

}
