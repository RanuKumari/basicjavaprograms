//Write a program to accept two integers and check whether they are equal or not.
package com.syne.Selection;

import java.util.Scanner;

public class CompareTwoIntegers {

	public static void main(String[] args) {
		int num1,num2;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number1: ");
		num1 = sc.nextInt();
		System.out.print("Enter number2: ");
		num2=sc.nextInt();
		if(num1==num2)
			System.out.println("Both are equal ");
		else
			System.out.println("Both are not equal ");
	}

}
