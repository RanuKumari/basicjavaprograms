
//WAP to convert distance in km into m, cm and mm.

package com.syne.Selection;

import java.util.Scanner;

public class ConversionDistance {

	public static void main(String[] args) {
		
		double km,mm,cm,m;
		System.out.println("Enter the distance in km ");
		Scanner sc =  new Scanner(System.in);
		km = sc.nextDouble();
		m=1000*km;
		cm = 100*m;
		mm = 10*cm;
		
		System.out.println("Kilometers: " +km);
		System.out.println("Meter: " +m);
		System.out.println("Centimeter: " +cm);
		System.out.println("Milimeter: " +mm);
		
	}

}
