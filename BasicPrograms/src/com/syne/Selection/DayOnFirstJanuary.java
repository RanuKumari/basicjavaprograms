//According to the Gregorian calendar, it was Monday on the date 01/01/1900. If any year is input through the keyboard 
//write a program to find out what is the day on 1st January of this year.
package com.syne.Selection;
import java.util.Scanner;

public class DayOnFirstJanuary {

	public static void main(String[] args) {
		int year,Total_days;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter year: ");
		year = sc.nextInt();
		int total_year = year-1900;
		String[] Array = {"Monday","Tuesday","Wednesday","Thursday", "Friday", "Saturday", "Sunday"};
		if(year%4==0 && year%100!=0 || year%400==0)
		{
			Total_days = total_year*366;
		}
		else
			Total_days = total_year*365;
			
		if(Total_days%7==0)
			System.out.println(Array[0]);
		else if(Total_days%7==1)
			System.out.println(Array[1]);
		else if(Total_days%7==2)
			System.out.println(Array[2]);
		else if(Total_days%7==3)
			System.out.println(Array[3]);
		else if(Total_days%7==4)
			System.out.println(Array[4]);
		else if(Total_days%7==5)
			System.out.println(Array[5]);
		else if(Total_days%7==6)
			System.out.println(Array[6]);
		
	}

}
