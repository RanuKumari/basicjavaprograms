//The current year and the year in which the employee joined the organization are entered through the keyboard. 
//If the number of years for which the employee has served the organization is greater than 3 then a bonus of Rs. 2500/- is given to the employee. 
//If the years of service are not greater than 3, then the program should do nothing.

package com.syne.Selection;

import java.util.Scanner;

public class EmployeeBonusCalculate {

	public static void main(String[] args) {
		int curr_year, join_year,sal;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Current year: ");
		curr_year = sc.nextInt();
		System.out.println("Enter Join year: ");
		join_year = sc.nextInt();
		System.out.println("Enter Salary of Employee: ");
		sal = sc.nextInt();
		
		int new_year = curr_year - join_year;
		if(new_year>3)
		{
			int bonus = 2500;
			sal = sal+bonus;
		}
		
		System.out.println("Total Salary of Employee is: "+sal);
		
		
		
	}

}
