package com.syne.Selection;

import java.util.Scanner;

public class FirstPositiveOddNumber {

	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range of sequence: ");
		n = sc.nextInt();
		System.out.println("First "+n+" positive natural number is: ");
		for(int i=1;i<=n;i++)
		{
			System.out.println(2*i-1);
		}
	}

}
