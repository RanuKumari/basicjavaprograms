package com.syne.Selection;

public class FloatDouble {

	public static void main(String[] args) {
		float x=0.7f; //float is flirt so x value can be changed so in case of float x will be 0.6999999
		if(x<0.7) //double(0.7) is more accurate than float so, 0.6999<0.7 will be true 
			System.out.println("GO and Study hard ... ");
		else
			System.out.println("Plan the party and lets enjoy");
	}

}
