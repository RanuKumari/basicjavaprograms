//In a company an employee is paid as under:
//If his basic salary is less than Rs. 1500, then HRA = 10% of basic salary and DA = 90% of basic salary. 
//If his salary is either equal to or above Rs. 1500, then HRA = Rs. 500 and DA = 98% of basic salary. 
//If the employee's salary is input through the keyboard write a program to find his gross salary
package com.syne.Selection;

import java.util.Scanner;

public class GrossSalaryOfEmployee {

	public static void main(String[] args) {
		
		int basic_salary, gross_salary;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Basic Salary of Employee: ");
		basic_salary = sc.nextInt();
		
		if(basic_salary<15000)
		{
			int HRA = (10*basic_salary/100);
			 int DA = (90*basic_salary/100);
			
			gross_salary = (int) (basic_salary+HRA+DA);
			
			//System.out.println(HRA);
			//System.out.println(DA);
			
			System.out.println("Gross Salary of Employee is: "+gross_salary);
		}
		if(basic_salary>=15000)
		{
			int HRA = 500;
			double DA = (98/100)*basic_salary;
			
			gross_salary = (int) (basic_salary+HRA+DA);
			
			System.out.println("Gross Salary of Employee is: "+gross_salary);
		}
	}

}
