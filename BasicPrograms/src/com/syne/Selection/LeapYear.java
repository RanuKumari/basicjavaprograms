//Any year is input through the keyboard. Write a program to determine whether the year is a leap year or not. (Hint: Use the % (modulus) operator)
package com.syne.Selection;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		int year;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter year: ");
		year = sc.nextInt();
		if(year%4==0 && year%100!=0 || year%400==0)
		{
			System.out.println("Leap year");
		}
		else
			System.out.println("Not a Leap year");
		
	}

}
