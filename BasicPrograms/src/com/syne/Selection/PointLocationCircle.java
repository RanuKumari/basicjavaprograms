package com.syne.Selection;

import java.util.Scanner;

public class PointLocationCircle {

	public static void main(String[] args) {
		int x,y,radius;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter coordinates: ");
		x = sc.nextInt();
		y = sc.nextInt();
		
		System.out.println("Enter Radius");
		radius = sc.nextInt();
		
		//int res =(int) Math.pow(x, 2) + (int) Math.pow(y, 2);
		//System.out.println("Pow"+res);
		
		if(((int) Math.pow(x, 2) + (int) Math.pow(y, 2))>(Math.pow(radius, 2)))
			System.out.println("Point lies outside the circle");
		else if(((int) Math.pow(x, 2) + (int) Math.pow(y, 2))<(Math.pow(radius, 2)))
			System.out.println("Point lies inside the circle");
		else if(((int) Math.pow(x, 2) + (int) Math.pow(y, 2))==(Math.pow(radius, 2)))
			System.out.println("Point lies on the circle");
	}

}
