//Given three points (x1, y1), (x2, y2) and (x3, y3), write a program to check if all the three points fall on one straight line
package com.syne.Selection;

import java.util.Scanner;

public class PointsOnLine {

	public static void main(String[] args) {
		int x1,y1,x2,y2,x3,y3;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter coordinates of point1 ");
		x1=sc.nextInt();
		y1=sc.nextInt();
		System.out.println("Enter coordinates of point2 ");
		x2=sc.nextInt();
		y2=sc.nextInt();
		System.out.println("Enter coordinates of point3 ");
		x3=sc.nextInt();
		y3=sc.nextInt();
		
		double m1 = (y2-y1)/(x2-x1);
		double m2 = (y3-y2)/(x3-x2);
		
		int res = Double.compare(m1, m2);
		
		if(res == 0)
			System.out.println("("+x1+","+y1+"), ("+x2+","+y2+"), ("+x3+","+y3+") Lies on Straight line");
		else
			System.out.println("("+x1+","+y1+"), ("+x2+","+y2+"), ("+x3+","+y3+") Don't Lies on Straight line");
		
	}

}
