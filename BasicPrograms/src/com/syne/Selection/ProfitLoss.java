
//If cost price and selling price of an item is input through the keyboard, 
//write a program to determine whether the seller has made profit or incurred loss. Also determine how much profit he made or loss he incurred.
package com.syne.Selection;

import java.util.Scanner;

public class ProfitLoss {

	public static void main(String[] args) {
		int cp, sp, profit, loss;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Cost Price of Item: ");
		cp = sc.nextInt();
		System.out.println("Enter Selling Price of Item: ");
		sp = sc.nextInt();
		if(sp>cp)
		{
			profit = sp-cp;
			System.out.println("Seller has made profit by: "+profit);
		}
		else
		{
			loss = cp-sp;
			System.out.println("Seller has made loss by: "+loss);
		}
	}

}
