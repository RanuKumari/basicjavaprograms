//A five-digit number is entered through the keyboard. 
//Write a program to obtain the reversed number and to determine whether the original and reversed numbers are equal or not. 
package com.syne.Selection;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		int num,rev=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number ");
		num=sc.nextInt();
		int temp = num;
		
		while(num>0)
		{
			int rem = num%10;
			rev=rev*10+rem;
			num=num/10;
		}
		System.out.println("Reverse number is: "+rev);
		if(temp==rev)
			System.out.println("original and reversed numbers are equal");
		else
			System.out.println("original and reversed numbers are not equal");
	}

}
