//print series 1 - 3 + 5 - 7 + 9 - 11 + 13 ... 2n-1

package com.syne.Selection;

import java.util.Scanner;

public class Series1 {

	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range of sequence: ");
		n = sc.nextInt();
		for(int i=1;i<=n;i++)
		{
			int c = 2*i-1;
			if(i%2==0)
			{
				System.out.print(-c+" +");
			}
			else
			{
				System.out.print(c+" ");
			}
		}
		
	}

}
