//The marks obtained by a student in 5 different subjects are input through the keyboard. 
//The student gets a division as per the following rules:
//Percentage above or equal to 60 - First division
//Percentage between 50 and 59 - Second division
//Percentage between 40 and 49 - Third division
//Percentage less than 40 - Fail
package com.syne.Selection;

import java.util.Scanner;

public class StudentDivision {

	public static void main(String[] args) {
		int m1,m2,m3,m4,m5,total_marks,marks_perc;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter marks 1");
		m1 = sc.nextInt();
		System.out.println("Enter marks 2");
		m2 = sc.nextInt();
		System.out.println("Enter marks 3");
		m3 = sc.nextInt();
		System.out.println("Enter marks 4");
		m4 = sc.nextInt();
		System.out.println("Enter marks 5");
		m5 = sc.nextInt();
		total_marks = m1+m2+m3+m4+m5;
		System.out.println(total_marks);
		marks_perc = (total_marks*100/500);
		System.out.println(marks_perc);
		if(marks_perc>=60)
			System.out.println("First Division");
		else if(marks_perc>50 && marks_perc<=59)
			System.out.println("Second division");
		else if(marks_perc>40 && marks_perc<=49)
			System.out.println("Second division");
		else if(marks_perc<=40)
			System.out.println("Fail");
		
	}

}
