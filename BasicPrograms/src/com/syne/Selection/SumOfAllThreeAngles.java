//Write a program to check whether a triangle is valid or not, when the three angles of the triangle are entered through the keyboard. 
//A triangle is valid if the sum of all the three angles is equal to 180 degrees.
package com.syne.Selection;

import java.util.Scanner;

public class SumOfAllThreeAngles {

	public static void main(String[] args) {
		int a1,a2,a3;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter angle1");
		a1=sc.nextInt();
		System.out.println("Enter angle2");
		a2=sc.nextInt();
		System.out.println("Enter angle3");
		a3=sc.nextInt();
		
		if(a1+a2+a3 == 180)
			System.out.println("Valid Triangle");
		else
			System.out.println("Invalid triangle");
	}

}
