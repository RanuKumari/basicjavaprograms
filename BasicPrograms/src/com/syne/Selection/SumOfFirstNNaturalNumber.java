package com.syne.Selection;

import java.util.Scanner;

public class SumOfFirstNNaturalNumber {

	public static void main(String[] args) {
		
		int n,sum=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range of sequence: ");
		n = sc.nextInt();
		System.out.println("First "+n+" positive natural number is: ");
		for(int i=1;i<=n;i++)
		{
			System.out.println(i);
			sum=sum+i;
		}
		System.out.println("Sum of first "+n+" natural number is: "+sum);
	}

}
