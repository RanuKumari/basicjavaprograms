package com.syne.Selection;

import java.util.Scanner;

public class TemperatureConversion {

	public static void main(String[] args) {
		double c,f;
		System.out.println("Enter Celsius");
		Scanner sc = new Scanner (System.in);
		c = sc.nextDouble();
		//f = 9/5*c+32; // logical error
		
		f = 1.8*c+32; // will work
		//f = (9/5*c)+32; //will not work
		//f = (c*9/5)+32; //will work
		
		System.out.println("Celsius = "+c);
		System.out.println("Farehenheit = "+f);
		
	}

}
