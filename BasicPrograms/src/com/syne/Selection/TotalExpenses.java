//While purchasing certain items, a discount of 10% is offered if the quantity purchased is more than 1000. 
//If quantity and price per item are input through the keyboard, write a program to calculate the total expenses.
package com.syne.Selection;

import java.util.Scanner;

public class TotalExpenses {

	public static void main(String[] args) {
		int d, total_expense,quant,price;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter total quantity of item: ");
		quant = sc.nextInt();
		System.out.println("Enter Price of item: ");
		price = sc.nextInt();
		
		total_expense = price*quant;
		if(total_expense>1000)
		{
			total_expense -= 10*(total_expense/100); 
			System.out.println("Total price is: "+total_expense);
			
		}
		else
			System.out.println("Total price is: "+total_expense);
			
			
	}

}
