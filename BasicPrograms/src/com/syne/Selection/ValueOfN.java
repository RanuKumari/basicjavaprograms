//Write a program to read the value of an integer m and display the value of n is 1 when m is larger than 0, 0 when m is 0 and -1 when m is less than 0.
package com.syne.Selection;

import java.util.Scanner;

public class ValueOfN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int m, n=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of m");
		m=sc.nextInt();
		if(m>0)
			n=1;
		else if(m==0)
			n=0;
		else if(m<0)
			n=-1;
		System.out.println("Value of n is "+n);

	}

}
