package com.syne.iteration;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		int n, first=0,second=1,temp;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter range of sequence: ");
		n = sc.nextInt();
		System.out.println("Fibonacci Series is: ");
		System.out.print(first+", ");
		//System.out.println(second+", ");
		for(int i=0;i<=n;i++)
		{
			temp=second;
			second=first+second;
			first=temp;
			if(i<n)
				System.out.print(first+", ");
			else
				System.out.print(first);
		}
	}

}
