package com.syne.iteration;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		int num, count=0;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number to check prime number: ");
		num = sc.nextInt();
		for(int i=1;i<=num;i++)
		{
			if(num%i==0)
			{
				count++;
			}
		}
		if(count<3 && num!=1)
			System.out.println("Prime Number");
		else
			System.out.println("Not Prime Number");
	}

}
