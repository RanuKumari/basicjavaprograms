//WAP TO SWAP TWO NUMBERS 
package com.syne.sequence;

public class NumberSwapping {

	public static void main(String[] args) {
		
		int a = 20, b = 10;
		
		System.out.println("a = "+a);
		System.out.println("b = "+b);

		/*int t = a;
		a = b;
		b = t;*/
		
		a = a + b;
		b = a - b;
		a = a - b;
		
		
		System.out.println("a = "+a);
		System.out.println("b = "+b);

	}

}
